var ctx = document.getElementById('chart').getContext('2d');

// chartData.push((chartData[chartData.length - 1] - 10));
// chartMonths[(chartData[chartData.length - 1] - 10)] = '';
// chartData.unshift(chartData[0] + 10);
// chartMonths[chartData[0] + 10] = '';
var chartLabels = [];
for (var key in chartMonths) {
    if (chartMonths.hasOwnProperty(key)) {
        chartLabels.push(chartMonths[key])
    }
}

smallTooltipEls = [];

var ctx = document.getElementById('chart').getContext("2d");
var gradientStroke = ctx.createLinearGradient(700, 0, 0, 0);
gradientStroke.addColorStop(0, "#FFFFFF");
gradientStroke.addColorStop(1, "#67E6CA");

var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: chartLabels,
        datasets: [{
            data: chartData,
            borderWidth: 1,
            borderColor: gradientStroke,
            backgroundColor: gradientStroke,
            pointBorderWidth: 4,
            pointBorderColor: '#fff',
            pointBackgroundColor: "#8798AD",
            pointRadius: 6,
            pointHoverBorderWidth: 4,
            pointHoverBorderColor: '#fff',
            pointHoverBackgroundColor: "#E35566",
            pointHoverRadius: 6,
            fill: 'start'
        }]
    },
    options: {
        animation: false,
        scales: {
            xAxes: [{
                display: false,
                min: -1,
                max: 10
            }],
            yAxes: [{
                display: false,
                ticks: {
                    min: chartData[chartData.length - 1] - 50
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false,

            custom: function (tooltipModel) {
                var tooltipEl = document.getElementById('chartjs-tooltip');

                // Create element on first render
                if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    tooltipEl.innerHTML = '<div class="tooltip-body"></div>';
                    document.body.appendChild(tooltipEl);
                }
                tooltipEl.classList.remove('last');
                tooltipEl.classList.remove('first');

                var tooltipEl2 = document.getElementById('chartjs-tooltip-2');

                if (!tooltipEl2) {
                    tooltipEl2 = document.createElement('div');
                    tooltipEl2.id = 'chartjs-tooltip-2';
                    tooltipEl2.innerHTML = '<div class="tooltip-body"></div>';
                    document.body.appendChild(tooltipEl2);
                }

                var staticTooltips = document.getElementsByClassName('static-tooltip');
                if (!staticTooltips.length) {
                    chartData.forEach(function (data, index) {
                        var tempEl = document.createElement('div');
                        tempEl.id = 'chartjs-static-tooltip-' + data;
                        tempEl.classList.add('static-tooltip');
                        if (index === 0) {
                            tempEl.classList.add('now');
                        }
                        tempEl.innerHTML = '<div class="tooltip-body">' + (index === 0 ? '<p class="now">Now</p>' : '')
                            + '<p>' + data + ' lbs</p></div>';

                        document.body.appendChild(tempEl);
                    });
                }

                // Hide if no tooltip
                if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                }

                function getBody(bodyItem) {
                    return bodyItem.lines;
                }

                // Set Text
                if (tooltipModel.body) {
                    var titleLines = tooltipModel.title || [];
                    var bodyLines = tooltipModel.body.map(getBody);

                    var innerHtml = '';
                    var el2 = false;

                    var bodyValue = 0;
                    bodyLines.forEach(function (body, i) {
                        if (parseInt(body) === currentChartValue)
                            el2 = true;

                        if (parseInt(body, 10) === chartData[chartData.length - 1])
                            tooltipEl.classList.add('last');
                        if (parseInt(body, 10) === chartData[0])
                            tooltipEl.classList.add('first');

                        bodyValue = parseInt(body, 10);
                        innerHtml += '<p class="weight">' + body + ' lbs</p><span class="bar"></span>';
                        innerHtml += '<p class="month">' + chartMonths[body] + '</p>'
                    });

                    var tableRoot = tooltipEl.querySelector('.tooltip-body');
                    var tableRoot2 = tooltipEl2.querySelector('.tooltip-body');
                    if (el2 === true) {
                        tableRoot2.innerHTML = innerHtml;
                    } else {
                        tableRoot.innerHTML = innerHtml;
                    }
                }

                // `this` will be the overall tooltip
                var position = this._chart.canvas.getBoundingClientRect();
                // var position = document.getElementById('chart-wrapper').getBoundingClientRect();

                if (el2 === true) {
                    tooltipEl2.style.opacity = 1;
                    tooltipEl2.style.position = 'absolute';
                    if (parseInt(tooltipModel.body.map(getBody)[0], 10) === chartData[chartData.length - 1])
                        tooltipEl2.style.left = position.left + window.pageXOffset + tooltipModel.caretX - tooltipEl2.getBoundingClientRect().width + 'px';
                    else if (parseInt(tooltipModel.body.map(getBody)[0], 10) !== chartData[0])
                        tooltipEl2.style.left = position.left + window.pageXOffset + tooltipModel.caretX - tooltipEl2.getBoundingClientRect().width / 2 + 'px';
                    else
                        tooltipEl2.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
                    tooltipEl2.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
                    tooltipEl2.style.pointerEvents = 'none';

                } else {
                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.position = 'absolute';
                    if (parseInt(tooltipModel.body.map(getBody)[0], 10) === chartData[chartData.length - 1])
                        tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - tooltipEl.getBoundingClientRect().width + 'px';
                    else if (parseInt(tooltipModel.body.map(getBody)[0], 10) !== chartData[0])
                        tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - tooltipEl.getBoundingClientRect().width / 2 + 'px';
                    else
                        tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
                    tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
                    tooltipEl.style.pointerEvents = 'none';
                }

                chartData.forEach(function (data, index) {
                    if (data === bodyValue) {
                        var tempEl = document.getElementById('chartjs-static-tooltip-' + data);
                        tempEl.style.opacity = 1;
                        tempEl.style.position = 'absolute';
                        if (parseInt(tooltipModel.body.map(getBody)[0], 10) === chartData[chartData.length - 1])
                            tempEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - tempEl.getBoundingClientRect().width + 'px';
                        else if (parseInt(tooltipModel.body.map(getBody)[0], 10) !== chartData[0])
                            tempEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - tempEl.getBoundingClientRect().width / 2 + 'px';
                        else
                            tempEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
                        tempEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
                        tempEl.style.pointerEvents = 'none';
                    }
                })
            }
        },
        layout: {
            padding: {
                top: 10,
                left: 10,
                right: 10
            }
        }
    },

});

t();


function t() {

    var meta = chart.getDatasetMeta(0),
        rect = chart.canvas.getBoundingClientRect(),
        node = chart.canvas;

    chartData.forEach(function (data, i) {
        var point = meta.data[i].getCenterPoint(),
            evt = new MouseEvent('mousemove', {
                clientX: rect.left + point.x,
                clientY: rect.top + point.y
            });
        node.dispatchEvent(evt)
    });

    evt = new MouseEvent('mousemove', {
        clientX: rect.left + 5,
        clientY: rect.top + 5
    });
    node.dispatchEvent(evt);

    $('#chartjs-tooltip').css('opacity', 0);
}

$(window).on('resize', function () {
    setTimeout(function () {
        t();
    }, 100);
});