var gulp = require('gulp'),
    sass = require('gulp-sass'),
    bower = require('gulp-bower'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    mainBowerFiles = require('main-bower-files'),
    gulpFilter = require('gulp-filter'),
    autoprefixer = require('gulp-autoprefixer'),
    css_nano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    print = require('gulp-print'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    order = require("gulp-order");

const assetsDir = 'assets';

gulp.task('init', function() {
    bower();
});

gulp.task('watch', function () {
    gulp.watch(assetsDir + '/scss/**/*.scss', ['sass']);
    gulp.watch(assetsDir + '/js/scripts/*.js', ['js']);
});

gulp.task('sass', function () {
    gulp.src(assetsDir + '/scss/style.scss')
        .pipe(plumber({errorHandler: notify.onError("Klaida: <%= error.message %>")}))
        .pipe(sass(
            {
                sourceComments: 'map',
                sourceMap: 'sass',
                imagePath: 'images'
            }
        ))
        .pipe(autoprefixer())
        .pipe(css_nano({zindex: false}))
        .pipe(rename('style.css'))
        .pipe(gulp.dest(assetsDir + '/css/'));
});

// gulp.task('default', ['serve']);

gulp.task('js', function() {
    gulp.src(assetsDir + '/js/scripts/*.js')
        .pipe(order([
            'functions.js',
            'onLoad.js',
        ]))
        .pipe(plumber({errorHandler: notify.onError("Klaida: <%= error.message %>")}))
        .pipe(sourcemaps.init())
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(assetsDir + '/js/'));
});

gulp.task('vendor:js', function () {
    var filterJS = gulpFilter('**/*.js', {restore: true});

    return gulp.src(mainBowerFiles(), {base: './bower_components'})
        .pipe(filterJS)
        .pipe(print())
        .pipe(concat('plugins.js'))
        .pipe(uglify())
        .pipe(gulp.dest(assetsDir + '/js/'));
});
gulp.task('vendor:css', function () {
    var filterCSS = gulpFilter('**/*.{css,scss}', {restore: true});

    return gulp.src(mainBowerFiles(), {base: './bower_components'})
        .pipe(filterCSS)
        .pipe(print())
        .pipe(sass())
        .pipe(concat('plugins.css'))
        .pipe(css_nano())
        .pipe(gulp.dest(assetsDir + '/css/'));
});

gulp.task('default', ['js','sass', 'watch']);